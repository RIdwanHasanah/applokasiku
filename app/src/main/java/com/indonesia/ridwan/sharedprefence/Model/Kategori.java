package com.indonesia.ridwan.sharedprefence.Model;

/**
 * Created by hasanah on 9/21/16.
 */
public class Kategori {

    String idkategori,kategoriNama,kategoriImage;

    public String getIdkategori() {
        return idkategori;
    }

    public void setIdkategori(String idkategori) {
        this.idkategori = idkategori;
    }

    public String getKategoriNama() {
        return kategoriNama;
    }

    public void setKategoriNama(String kategoriNama) {
        this.kategoriNama = kategoriNama;
    }

    public String getKategoriImage() {
        return kategoriImage;
    }

    public void setKategoriImage(String kategoriImage) {
        this.kategoriImage = kategoriImage;
    }
}
