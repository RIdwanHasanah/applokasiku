package com.indonesia.ridwan.sharedprefence;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.indonesia.ridwan.sharedprefence.Adapter.KategoriAdapter;
import com.indonesia.ridwan.sharedprefence.Fragments.LokasiFragment;
import com.indonesia.ridwan.sharedprefence.Helpers.BaseActivity;
import com.indonesia.ridwan.sharedprefence.Helpers.FileUtility;
import com.indonesia.ridwan.sharedprefence.Helpers.ImageCustomize;
import com.indonesia.ridwan.sharedprefence.Helpers.RbHelper;
import com.indonesia.ridwan.sharedprefence.Model.Desa;
import com.indonesia.ridwan.sharedprefence.Model.Jenis;
import com.indonesia.ridwan.sharedprefence.Model.Kategori;
import com.indonesia.ridwan.sharedprefence.Model.Kecamatan;
import com.indonesia.ridwan.sharedprefence.Model.Kota;
import com.schibstedspain.leku.LocationPickerActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AddLokasi extends BaseActivity {

    private ImageView ivCamera,ivPreview;
    private EditText txtNama;
    private Button btnSimpan;

    private FileUtility fileUtility;
    private final int TAKE_PHOTO = 234;
    private static final int SELECT_PHOTO = 321 ;

    private boolean isImage = false;
    private  String path = "";

    private Spinner spKota,spDesa,spKecamatan,spJenis,spKategori;
    private EditText txtAlamat,txtFasilitas,txtKet,txtKoorniat;

    private ArrayList<String> dKategoriText = new ArrayList<>();
    private ArrayList<Kategori> dKategori = new ArrayList<>();

    private ArrayList<String> dJenisText = new ArrayList<>();
    private ArrayList<Jenis> dJenis = new ArrayList<>();

    private ArrayList<String> dKotaText = new ArrayList<>();
    private ArrayList<Kota> dKota = new ArrayList<>();

    private ArrayList<String> dDesaText = new ArrayList<>();
    private ArrayList<Desa> dDesa = new ArrayList<>();

    private ArrayList<String> dKecamatanText = new ArrayList<>();
    private ArrayList<Kecamatan> dKecamatan = new ArrayList<>();

    String idKategori,idKota,idDesa,idKecamatan,idJenis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_lokasi);

        menuDrawer(3);


        setupView();


    }

    private void setupView(){

        ivCamera = (ImageView) findViewById(R.id.imageView3);
        ivPreview = (ImageView) findViewById(R.id.imageView2);
        ivCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.startAnimation(animation);
                //tampilkan pilihan image dari mana
                showDialogPilih();
            }
        });

        txtNama = (EditText) findViewById(R.id.eKategori);

        btnSimpan = (Button) findViewById(R.id.btnSimpan);
        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                simpanAction();
            }
        });

        spDesa = (Spinner) findViewById(R.id.spDesa);
        spKota = (Spinner) findViewById(R.id.spKota);
        spJenis = (Spinner) findViewById(R.id.spJenis);
        spKecamatan = (Spinner) findViewById(R.id.spKecamatan);
        spKategori = (Spinner) findViewById(R.id.spKategori);

        txtNama = (EditText) findViewById(R.id.eLokasi);
        txtAlamat = (EditText) findViewById(R.id.eAlamat);
        txtFasilitas = (EditText) findViewById(R.id.eFasilitas);
        txtKet = (EditText) findViewById(R.id.eDeskripsi);
        txtKoorniat = (EditText) findViewById(R.id.eKoordinat);
        txtKoorniat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(c, LocationPickerActivity.class);
                startActivityForResult(i, 1);
            }
        });

        txtKoorniat.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b){
                    Intent i = new Intent(c, LocationPickerActivity.class);
                    i.putExtra(LocationPickerActivity.LATITUDE,-6.130711);
                    i.putExtra(LocationPickerActivity.LONGITUDE,106.8466554);
                    startActivityForResult(i, 1);

                }
            }
        });

        getData();

    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(getBaseContext(),MainActivity.class);
        startActivity(i);
        finish();
    }

    private void showDialogPilih(){
        String[] items = {"Kamera","Galeri"};

        AlertDialog.Builder builder = new AlertDialog.Builder(c);

        builder.setTitle("Pilih Gambar");

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i){
                    case 0:
                        callKamera();
                        break;
                    case 1:
                        callGaleri();
                        break;
                }
            }
        });

        /**Menampilkan Dialog */
        //builder.show();
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    /**Method callGalery untuk mengambil gambar di galery */
    private void callGaleri(){
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        i.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivityForResult(i,SELECT_PHOTO);
    }

    /**Method callKamera untuk mengambil gambar lewat camera*/
    private void callKamera(){
        fileUtility = new FileUtility(c);
        Uri uriSavedImage = Uri.fromFile(fileUtility.getTempJpgImageFile());

        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        i.putExtra(MediaStore.EXTRA_OUTPUT,uriSavedImage);
        i.setFlags(i.FLAG_ACTIVITY_NO_HISTORY);
        startActivityForResult(i,TAKE_PHOTO);
    }

    //untuk handle gambar yang di pilih
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode){
            case TAKE_PHOTO:
                if(resultCode== Activity.RESULT_OK){
                    File imageFile = fileUtility.getTempJpgImageFile();
                    String path = imageFile.getAbsolutePath();

                    //memanggil lokasi file
                    File file = new File(path);

                    File filetmp = ImageCustomize.resizeImage(file);
                    Bitmap bmp = ImageCustomize.decodeFile(filetmp,300);

                    ivPreview.setImageBitmap(bmp);

                    isImage = true;
                }else {
                    isImage = false;
                }
                break;

            case SELECT_PHOTO:
                if (resultCode == Activity.RESULT_OK) {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int index = cursor.getColumnIndex(filePathColumn[0]);
                    String pictPath = cursor.getString(index);
                    cursor.close();

                    File f = new File(pictPath);
                    File file1 = ImageCustomize.resizeImage(f);

                    Bitmap bmp = ImageCustomize.decodeFile(file1, 300);

                    ivPreview.setImageBitmap(bmp);

                    path = file1.getAbsolutePath().toString();
                    isImage = true;
                }else {
                    isImage = false;
                }
                break;
            case 1:
                if(resultCode==RESULT_OK){
                    double latitude = data.getDoubleExtra(LocationPickerActivity.LATITUDE, 0);
                    Log.d("LATITUDE****", String.valueOf(latitude));
                    double longitude = data.getDoubleExtra(LocationPickerActivity.LONGITUDE, 0);
                    Log.d("LONGITUDE****", String.valueOf(longitude));
                    String address = data.getStringExtra(LocationPickerActivity.LOCATION_ADDRESS);
                    Log.d("ADDRESS****", String.valueOf(latitude)+","+String.valueOf(longitude));
                    txtKoorniat.setText(String.valueOf(latitude)+","+String.valueOf(longitude));

                }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }



    public void simpanAction(){

        txtNama.setError(null);
        txtKoorniat.setError(null);
        txtAlamat.setError(null);
        txtFasilitas.setError(null);
        txtKet.setError(null);

        //lakukan proses pengecekan semua field sudah disi dengan benar
        boolean cancel = false;
        View focusView= null;

        if (RbHelper.isEmpty(txtNama)){

            txtNama.setError("Kategori harus disisi");
            focusView  = txtNama;
            cancel = true;
        }else if (RbHelper.isEmpty(txtAlamat)){

            txtAlamat.setError("Alamat harus disisi");
            focusView  = txtAlamat;
            cancel = true;
        }else if (RbHelper.isEmpty(txtFasilitas)){

            txtFasilitas.setError("Fasilitas harus disisi");
            focusView  = txtFasilitas;
            cancel = true;
        }else if (RbHelper.isEmpty(txtKet)){

            txtKet.setError("Deskripsi Belum disisi");
            focusView  = txtKet;
            cancel = true;
        }else if (RbHelper.isEmpty(txtKoorniat)){

            txtKoorniat.setError("Koordinat harus disisi");
            focusView  = txtKoorniat;
            cancel = true;
        }


        //cek apakah ada yang belum diisi
        if (cancel){
            focusView.requestFocus();
        }else {

            String url = RbHelper.BASE_URL + "lokasi/tambah";

            //siapkan parameter yang akan di kirim
            RequestBody formbody = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("f_nama",txtNama.getText().toString())
                    .addFormDataPart("f_idUser",sesi.getIdUser())
                    .addFormDataPart("f_kategori",idKategori)
                    .addFormDataPart("f_jenis",idJenis)
                    .addFormDataPart("f_kota",idKota)
                    .addFormDataPart("f_kecamatan",idKecamatan)
                    .addFormDataPart("f_desa",idDesa)
                    .addFormDataPart("f_alamat",txtAlamat.getText().toString())
                    .addFormDataPart("f_fasilitas",txtFasilitas.getText().toString())
                    .addFormDataPart("f_deskripsi",txtKet.getText().toString())
                    .addFormDataPart("f_lokasi",txtKoorniat.getText().toString())
                    .addFormDataPart("userfile","lokasi_foto.png",
                            RequestBody.create(MediaType.parse("image/png"),new File(path)))
                    .build();
            // f_nama,f_idUser,f_kategori,f_jenis,f_kota,f_kecamatan,f_desa,f_alamat,f_fasilitas,f_deskripsi',f_lokasi, userfile

            //buat request yang akan di kirim keserver
            final Request request = new Request.Builder()
                    .url(url)
                    .post(formbody)
                    .build();


            //debuging data yang dikirimkan

            RbHelper.pre("url : "+url+",parameter"+RbHelper.bodyToString(formbody));

            //tampilkan loadingnya
            showLoading();

            //kirimkan ke server
            okhttp.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, final IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                hideLoading();
                                RbHelper.pesan(c,"Error get data" + e.getMessage());
                            }catch (Exception ex){


                            }
                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                hideLoading();
                            }catch (Exception e){

                            }
                        }
                    });

                    if (!response.isSuccessful()){
                        throw new IOException("Unexpected code"+response);
                    }

                    //baca response dari server
                    final String reponData = response.body().string();
                    RbHelper.pre("respon "+reponData);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                JSONObject json = new JSONObject(reponData);

                                //cek resultnya
                                boolean hasil = json.getBoolean("result");
                                String msg = json.getString("msg");



                                if (hasil){

                                    //create sesion loginya

                                    RbHelper.pesan(c,msg);
                                    Intent i = new Intent(c,LokasiFragment.class);
                                    startActivity(i);
                                    finish();
                                }
                            }catch (JSONException e){
                                e.printStackTrace();
                                RbHelper.pesan(c,"Error pasrsing Json" + e.getMessage());
                            }catch (Exception e){
                                e.printStackTrace();
                                RbHelper.pesan(c,"Error get data" + e.getMessage());
                            }
                        }
                    });

                }
            });

        }
    }

    private void getData(){
        dKategori.clear();
        dKategoriText.clear();

        String url = RbHelper.BASE_URL+"kategori";

        //Create request okHtttp3
        Request request = new Request.Builder()
                .url(url)
                .build();

        RbHelper.pre("url : "+url);

        OkHttpClient okHttpClient = new OkHttpClient();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call,final IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        RbHelper.pesan(c,"Error Connection"+e.getMessage());
                    }
                });

            }

            @Override
            public void onResponse(final Call call, Response response) throws IOException {

                //chek hasil response
                if(!response.isSuccessful()){
                    throw new IOException("Unexpected code"+response);
                }
                //read data
                final String resposeData = response.body().string();
                RbHelper.pre("respone: "+resposeData );

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject json = new JSONObject(resposeData);
                            //chek result
                            boolean hasil = json.getBoolean("result");
                            String msg = json.getString("msg");

                            if (hasil){
                                JSONArray jsonArray = json.getJSONArray("data");
                                for (int i=0;i<jsonArray.length();i++){
                                    JSONObject jObj = jsonArray.getJSONObject(i);

                                    Kategori x = new Kategori();
                                    x.setIdkategori(jObj.getString("id_kategori"));
                                    x.setKategoriNama(jObj.getString("kategori_nama"));
                                    x.setKategoriImage(jObj.getString("kategori_image"));

                                    dKategori.add(x);
                                    dKategoriText.add(x.getKategoriNama());
                                }
                            }

                            ArrayAdapter adapter = new ArrayAdapter(c,android.R.layout.simple_list_item_1,
                                    dKategoriText);
                            spKategori.setAdapter(adapter);

                            spKategori.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    Kategori x = dKategori.get(i);
                                    idKategori = x.getIdkategori();
                                    getDataJenis();
                                    getDataKota();

                                }


                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                            RbHelper.pesan(c,"Error Parsing Json");
                        }catch (Exception e){
                            e.printStackTrace();
                            RbHelper.pesan(c,"Error ambil data");
                        }
                    }
                });
            }
        });
    }

    private void getDataJenis(){
        dJenis.clear();
        dJenisText.clear();

        String url = RbHelper.BASE_URL+"jenis/jenis_by_kategori/"+idKategori;

        //Create request okHtttp3
        Request request = new Request.Builder()
                .url(url)
                .build();

        RbHelper.pre("url : "+url);

        OkHttpClient okHttpClient = new OkHttpClient();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        RbHelper.pesan(c,"Error Connection"+e.getMessage());
                    }
                });

            }

            @Override
            public void onResponse(final Call call, Response response) throws IOException {

                //chek hasil response
                if(!response.isSuccessful()){
                    throw new IOException("Unexpected code"+response);
                }
                //read data
                final String resposeData = response.body().string();
                RbHelper.pre("respone: "+resposeData );

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject json = new JSONObject(resposeData);
                            //chek result
                            boolean hasil = json.getBoolean("result");
                            String msg = json.getString("msg");

                            if (hasil){
                                JSONArray jsonArray = json.getJSONArray("data");
                                for (int i=0;i<jsonArray.length();i++){
                                    JSONObject jObj = jsonArray.getJSONObject(i);

                                    Jenis x = new Jenis();
                                    x.setIdJenis(jObj.getString("id_jenis"));
                                    x.setJenisNama(jObj.getString("jenis_nama"));
                                    x.setJenisImage(jObj.getString("jenis_image"));

                                    dJenis.add(x);
                                    dJenisText.add(x.getJenisNama());
                                }
                            }

                            ArrayAdapter adapter = new ArrayAdapter(c,android.R.layout.simple_list_item_1,
                                    dJenisText);
                            spJenis.setAdapter(adapter);

                            spJenis.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    Jenis x = dJenis.get(i);
                                    idJenis = x.getIdJenis();
                                }


                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                            RbHelper.pesan(c,"Error Parsing Json");
                        }catch (Exception e){
                            e.printStackTrace();
                            RbHelper.pesan(c,"Error ambil data");
                        }
                    }
                });
            }
        });
    }

    private void getDataKota(){
        dKota.clear();
        dKotaText.clear();

        String url = RbHelper.BASE_URL+"referensi/get_kota";

        //Create request okHtttp3
        Request request = new Request.Builder()
                .url(url)
                .build();

        RbHelper.pre("url : "+url);

        OkHttpClient okHttpClient = new OkHttpClient();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        RbHelper.pesan(c,"Error Connection"+e.getMessage());
                    }
                });

            }

            @Override
            public void onResponse(final Call call, Response response) throws IOException {

                //chek hasil response
                if(!response.isSuccessful()){
                    throw new IOException("Unexpected code"+response);
                }
                //read data
                final String resposeData = response.body().string();
                RbHelper.pre("respone: "+resposeData );

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject json = new JSONObject(resposeData);
                            //chek result
                            boolean hasil = json.getBoolean("result");
                            String msg = json.getString("msg");

                            if (hasil){
                                JSONArray jsonArray = json.getJSONArray("data");
                                for (int i=0;i<jsonArray.length();i++){
                                    JSONObject jObj = jsonArray.getJSONObject(i);

                                    Kota x = new Kota();
                                    x.setIdKota(jObj.getString("id_kabupaten"));
                                    x.setNmKota(jObj.getString("kabupaten_nama"));


                                    dKota.add(x);
                                    dKotaText.add(x.getNmKota());
                                }
                            }

                            ArrayAdapter adapter = new ArrayAdapter(c,android.R.layout.simple_list_item_1,
                                    dKotaText);
                            spKota.setAdapter(adapter);

                            spKota.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    Kota x = dKota.get(i);
                                    idKota = x.getIdKota();
                                    getDataKecamtan();

                                }


                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                            RbHelper.pesan(c,"Error Parsing Json");
                        }catch (Exception e){
                            e.printStackTrace();
                            RbHelper.pesan(c,"Error ambil data");
                        }
                    }
                });
            }
        });
    }

    private void getDataKecamtan(){
        dKecamatan.clear();
        dKecamatanText.clear();

        String url = RbHelper.BASE_URL+"referensi/get_kecamatan/"+idKota;

        //Create request okHtttp3
        Request request = new Request.Builder()
                .url(url)
                .build();

        RbHelper.pre("url : "+url);

        OkHttpClient okHttpClient = new OkHttpClient();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        RbHelper.pesan(c,"Error Connection"+e.getMessage());
                    }
                });

            }

            @Override
            public void onResponse(final Call call, Response response) throws IOException {

                //chek hasil response
                if(!response.isSuccessful()){
                    throw new IOException("Unexpected code Kecamatan"+response);
                }
                //read data
                final String resposeData = response.body().string();
                RbHelper.pre("respone: "+resposeData );

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject json = new JSONObject(resposeData);
                            //chek result
                            boolean hasil = json.getBoolean("result");
                            String msg = json.getString("msg");

                            if (hasil){
                                JSONArray jsonArray = json.getJSONArray("data");
                                for (int i=0;i<jsonArray.length();i++){
                                    JSONObject jObj = jsonArray.getJSONObject(i);

                                    Kecamatan x = new Kecamatan();
                                    x.setIdKecamatan(jObj.getString("id_kecamatan"));
                                    x.setNamaKecamatan(jObj.getString("kecamatan_nama"));


                                    dKecamatan.add(x);
                                    dKecamatanText.add(x.getNamaKecamatan());
                                }
                            }

                            ArrayAdapter adapter = new ArrayAdapter(c,android.R.layout.simple_list_item_1,
                                    dKecamatanText);
                            spKecamatan.setAdapter(adapter);

                            spKecamatan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    Kecamatan x = dKecamatan.get(i);
                                    idKecamatan = x.getIdKecamatan();
                                    getDataDesa();

                                }


                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                            RbHelper.pesan(c,"Error Parsing Json Kecamatan");
                        }catch (Exception e){
                            e.printStackTrace();
                            RbHelper.pesan(c,"Error ambil data Kecamatan");
                        }
                    }
                });
            }
        });
    }

    private void getDataDesa(){
        dDesa.clear();
        dDesaText.clear();

        String url = RbHelper.BASE_URL+"referensi/get_desa/"+idKecamatan;

        //Create request okHtttp3
        Request request = new Request.Builder()
                .url(url)
                .build();

        RbHelper.pre("url : "+url);

        OkHttpClient okHttpClient = new OkHttpClient();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        RbHelper.pesan(c,"Error Connection"+e.getMessage());
                    }
                });

            }

            @Override
            public void onResponse(final Call call, Response response) throws IOException {

                //chek hasil response
                if(!response.isSuccessful()){
                    throw new IOException("Unexpected code"+response);
                }
                //read data
                final String resposeData = response.body().string();
                RbHelper.pre("respone: "+resposeData );

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject json = new JSONObject(resposeData);
                            //chek result
                            boolean hasil = json.getBoolean("result");
                            String msg = json.getString("msg");

                            if (hasil){
                                JSONArray jsonArray = json.getJSONArray("data");
                                for (int i=0;i<jsonArray.length();i++){
                                    JSONObject jObj = jsonArray.getJSONObject(i);

                                    Desa x = new Desa();
                                    x.setIdDesa(jObj.getString("id_desa"));
                                    x.setNmDesa(jObj.getString("desa_nama"));


                                    dDesa.add(x);
                                    dDesaText.add(x.getNmDesa());
                                }
                            }

                            ArrayAdapter adapter = new ArrayAdapter(c,android.R.layout.simple_list_item_1,
                                    dDesaText);
                            spDesa.setAdapter(adapter);

                            spDesa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                                    Desa x = dDesa.get(i);
                                    idDesa = x.getIdDesa();
                                    getDataKecamtan();
                                }


                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                            RbHelper.pesan(c,"Error Parsing Json");
                        }catch (Exception e){
                            e.printStackTrace();
                            RbHelper.pesan(c,"Error ambil data");
                        }
                    }
                });
            }
        });
    }
/**
 * JIka tejadi Error di gradle maka ganti
 * compile 'com.google.android.gms:play-services-maps:9.4.0' menjadi
 * compile 'com.google.android.gms:play-services-maps:8.4.0'*/

    /**
     * Buat settexxt untuk alamat*/

}
