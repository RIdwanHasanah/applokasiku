package com.indonesia.ridwan.sharedprefence.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indonesia.ridwan.sharedprefence.Helpers.RbHelper;
import com.indonesia.ridwan.sharedprefence.Model.Lokasi;
import com.indonesia.ridwan.sharedprefence.R;

import java.util.ArrayList;

/**
 * Created by hasanah on 9/22/16.
 */
public class LokasiAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Lokasi> data;
    private Context c;

    public  LokasiAdapter (Context c,ArrayList<Lokasi> data){
        this.data= data;
        this.c = c;
    }

    @Override
    //mencari Layout
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(c).inflate(R.layout.row_lokasi,parent,false);
        return new MyViewHolder(v);
    }

    @Override
    //memasukan data
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        //memamsukan data kedalam masing2 item
        Lokasi x = data.get(position);
        MyViewHolder mHolder = (MyViewHolder)holder;
        mHolder.tvLokasi.setText(x.getLokasiNama());
        mHolder.tvLokasiDesa.setText(x.getLokasiDesa());
        mHolder.nmWisata.setText(x.getNmWisata());
        mHolder.seen.setText("Seen "+x.getLokasiSeen());

        RbHelper.downloadImage(c,RbHelper.BASE_URL_IMAGE+x.getLokasiImg(),mHolder.imglokasi);
    }

    @Override
    public int getItemCount() {
        return data==null ? 0 : data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView imglokasi;
        TextView tvLokasi,tvLokasiDesa,nmWisata,seen;
        RelativeLayout container;

        public MyViewHolder(View v){
            super(v);
            imglokasi = (ImageView) v.findViewById(R.id.imglokasi);
            tvLokasi = (TextView) v.findViewById(R.id.tvlokasi);
            tvLokasiDesa = (TextView) v.findViewById(R.id.tvLokasiDesa);
            nmWisata = (TextView) v.findViewById(R.id.nmWisata);
            seen = (TextView) v.findViewById(R.id.seen);
        }


    }
}
