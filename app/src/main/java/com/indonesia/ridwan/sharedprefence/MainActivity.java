package com.indonesia.ridwan.sharedprefence;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.indonesia.ridwan.sharedprefence.Adapter.ViewPagerAdapter;
import com.indonesia.ridwan.sharedprefence.Helpers.BaseActivity;
import com.indonesia.ridwan.sharedprefence.Helpers.RbHelper;

import java.util.ArrayList;

import it.neokree.materialtabs.MaterialTab;
import it.neokree.materialtabs.MaterialTabHost;
import it.neokree.materialtabs.MaterialTabListener;

public class MainActivity extends BaseActivity implements MaterialTabListener {
    private MaterialTabHost tabHost;
    private ViewPager pager;
    private ViewPagerAdapter adapter;
    private ArrayList<String> namaTab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        menuDrawer(1);

        setupView();

        //RbHelper.pesan(c,sesi.getEmail());
    }

    private void setupView(){
        tabHost = (MaterialTabHost) findViewById(R.id.materialTabHost);
        pager = (ViewPager)findViewById(R.id.viewPager);
        //action untuk pager
          pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
              @Override
              public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

              }

              @Override
              public void onPageSelected(int position) {
                  //memilih Navigasi page yang di klik mk akan pindah ke posisi tab yang di klik
                  tabHost.setSelectedNavigationItem(position);
              }

              @Override
              public void onPageScrollStateChanged(int state) {

              }
          });
        //Memeberikan Nama pada Tablayout
        namaTab = new ArrayList<>();
        namaTab.add("Kategori");
        namaTab.add("Lokasi");
        namaTab.add("Peta");

        //for viewpager
        //Menghubungkan Viepager d Xml dengan Fragment
        adapter = new ViewPagerAdapter(getSupportFragmentManager(),namaTab);
        pager.setAdapter(adapter);

        //tambahkan judul untuk tab
        for (int i=0;i<adapter.getCount();i++){
            tabHost.addTab(tabHost.newTab()
                .setText(adapter.getPageTitle(i))
                .setTabListener(this)
            );
        }
    }

    @Override
    public void onTabSelected(MaterialTab tab) {
        pager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabReselected(MaterialTab tab) {

    }

    @Override
    public void onTabUnselected(MaterialTab tab) {

    }

    /*public void onKlik(View v){

        sesi.logout();

        Intent i = new Intent(c,LoginActivity.class);
        startActivity(i);
        finish();

    }*/
}