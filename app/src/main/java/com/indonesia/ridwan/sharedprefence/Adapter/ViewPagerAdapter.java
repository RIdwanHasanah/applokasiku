package com.indonesia.ridwan.sharedprefence.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.indonesia.ridwan.sharedprefence.Fragments.KategoriFragment;
import com.indonesia.ridwan.sharedprefence.Fragments.LokasiFragment;
import com.indonesia.ridwan.sharedprefence.Fragments.PetaFragment;

import java.util.ArrayList;

/**
 * Created by hasanah on 9/21/16.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<String> data;

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public ViewPagerAdapter(FragmentManager fm,ArrayList<String> data) {
        super(fm);
        this.data=data;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new KategoriFragment();
            case 1:
                return new LokasiFragment();
            case 2 :
                return new PetaFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return data.get(position);
    }
}
