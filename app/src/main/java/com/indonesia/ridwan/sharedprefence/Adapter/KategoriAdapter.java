package com.indonesia.ridwan.sharedprefence.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.indonesia.ridwan.sharedprefence.Helpers.RbHelper;
import com.indonesia.ridwan.sharedprefence.Model.Kategori;
import com.indonesia.ridwan.sharedprefence.R;

import java.util.ArrayList;

/**
 * Created by hasanah on 9/21/16.
 */
public class KategoriAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //ArrayList di isikan parameter model Kategori
    private ArrayList<Kategori> data;
    private Context c;

    //construktor
    public KategoriAdapter(Context c,ArrayList<Kategori>data){
        this.c = c;
        this.data=data;
    }

    @Override
    //onCreate untuk mencari layout
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(c).inflate(R.layout.row_kategori,parent,false);
        return new MyViewHolder(v);
    }

    @Override
    //onBindViewHolder untuk memasukan data
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        //masukan Masing2 data ke dalam item
        Kategori x = data.get(position);
        MyViewHolder mHolder = (MyViewHolder)holder;
        mHolder.tvJudul.setText(x.getKategoriNama());

        Glide.with(c)
                .load(RbHelper.BASE_URL_IMAGE+"kategori/"+x.getKategoriImage())
                .centerCrop()
                .placeholder(R.drawable.ic_lokasi)
                .crossFade()
                .into(mHolder.ivCover);


    }

    @Override
    //untuk mengecek data jika null
    //fungsi tanda tanya (?) itu sama seperti if
    public int getItemCount() {
        return data==null ? 0 : data.size();
    }

    //siapkan VIew Holder

    class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView ivCover;
        TextView tvJudul;
        RelativeLayout container;

        public MyViewHolder(View v){
            super(v);
            ivCover = (ImageView) v.findViewById(R.id.ivCover);
            tvJudul = (TextView)v.findViewById(R.id.tvJudul);
            container = (RelativeLayout)v.findViewById(R.id.container);
        }

    }
}
