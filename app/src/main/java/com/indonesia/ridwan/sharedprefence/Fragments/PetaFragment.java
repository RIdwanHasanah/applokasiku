package com.indonesia.ridwan.sharedprefence.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.indonesia.ridwan.sharedprefence.Adapter.LokasiAdapter;
import com.indonesia.ridwan.sharedprefence.Helpers.RbHelper;
import com.indonesia.ridwan.sharedprefence.Model.Lokasi;
import com.indonesia.ridwan.sharedprefence.Model.Peta;
import com.indonesia.ridwan.sharedprefence.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PetaFragment extends Fragment implements OnMapReadyCallback{
    private GoogleMap map;
    private MapView mapView;
    private LatLng posisiAwal,gunungtilu;
    private MarkerOptions markerOptions;
    private Double currentLat,currentLng;

    //siapkan dta untuk menampung data lokasi
    private ArrayList<Lokasi> data;

    public PetaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_peta,container,false);

        data = new ArrayList<>();

        setMap(view);
        return view;
    }

    private void setMap(View view){
        SupportMapFragment mapFragment = (SupportMapFragment)getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //definisikan marker
        markerOptions = new MarkerOptions();

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        //saat map ready kita konfigurasi
        map = googleMap;
        //type map
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(true);

        //tambahkan posisi awal
        posisiAwal = new LatLng(-7.0768157,107.9365455);
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(posisiAwal,16));

        addMarker(-7.0768157,107.9365455,"Posisi Anda");
        getData();

        addMarker(-6.129108,106.8054374,"pademagan");

    }

    //method untuk menampilkan marker
    private void addMarker(Double lat, Double lng, String judul){
        //kita clear map
        //map.clear();

        markerOptions.position(new LatLng(lat,lng));
        markerOptions.title(judul).snippet("");

        //tambahkan maker custom
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.male))
                .anchor(0.5f,0.5f);

        //tambahkan marker ke posisi
        //map.animateCamera(CameraUpdateFactory.newLatLngZoom(posisiAwal,16));
        map.addMarker(markerOptions);

    }

    //method untuk menampilkan marker
    private void addMarker(Double lat, Double lng, String judul, int jenis, String kategori){
        //kita clear map
        //map.clear();

        markerOptions.position(new LatLng(lat,lng));
        markerOptions.title(judul).snippet(kategori);

        if(jenis ==1){
            //tambahkan maker custom
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.lake))
                    .anchor(0.5f,0.5f);
        } else if(jenis ==2){
            //tambahkan maker custom
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.waterfall))
                    .anchor(0.5f,0.5f);
        } else if(jenis ==3){
            //tambahkan maker custom
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.riparianhabitat))
                    .anchor(0.5f,0.5f);
        } else if(jenis ==4){
            //tambahkan maker custom
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.museum_science))
                    .anchor(0.5f,0.5f);
        }else if(jenis ==5){
            //tambahkan maker custom
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.swimming))
                    .anchor(0.5f,0.5f);
        } else if(jenis ==6){
            //tambahkan maker custom
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.dam))
                    .anchor(0.5f,0.5f);
        }else if(jenis ==7){
            //tambahkan maker custom
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.mountains))
                    .anchor(0.5f,0.5f);
        }



        //tambahkan marker ke posisi
        //map.animateCamera(CameraUpdateFactory.newLatLngZoom(posisiAwal,16));
        map.addMarker(markerOptions);

    }

    private void getData(){

        data.clear();

        String url = RbHelper.BASE_URL+"lokasi/peta";

        //Create okhttp3
        Request request = new Request.Builder()
                .url(url)
                .build();

        RbHelper.pre("url: " +url);


        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {


                    }
                });
                RbHelper.pesan(getActivity(),"Error Connection"+e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {


                //chek hasil response
                if (!response.isSuccessful()){
                    throw  new IOException("Unexpected code"+response);
                }
                //read data
                final String responseData = response.body().string();
                RbHelper.pre("response: "+responseData);

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject json = new JSONObject(responseData);
                            //check result
                            boolean hasil = json.getBoolean("result");
                            String msg = json.getString("msg");

                            if (hasil){
                                JSONArray jsonArray = json.getJSONArray("data");
                                for (int i=0;i<jsonArray.length();i++){
                                    JSONObject jObj= jsonArray.getJSONObject(i);

                                    Lokasi x = new Lokasi();
                                    x.setLalitude(jObj.getString("lokasi_latitude"));
                                    x.setLonglitude(jObj.getString("lokasi_longitude"));
                                    x.setIdLokasi(jObj.getString("id_lokasi"));
                                    x.setLokasiNama(jObj.getString("lokasi_nama"));
                                    x.setLokasiSeen(jObj.getString("lokasi_see"));
                                    x.setLokasiImg(jObj.getString("lokasi_gambar"));
                                    x.setLokasiDesa(jObj.getString("desaNama"));
                                    x.setNmWisata(jObj.getString("jenis_nama"));
                                    x.setMarker(jObj.getString("jenis_marker"));
                                    x.setIdJenis(jObj.getString("id_jenis"));


                                    data.add(x);

                                    //tambahkan marketnya
                                    addMarker(Double.parseDouble(x.getLalitude()),
                                            Double.parseDouble(x.getLonglitude()),
                                            x.getLokasiNama(),
                                            Integer.parseInt(x.getIdJenis()),
                                            x.getNmWisata());
                                }

                                map.addMarker(markerOptions);
                            }


                        }catch (JSONException e){
                            e.printStackTrace();
                            RbHelper.pesan(getActivity(),"Error Parsing json");
                        }catch (Exception ex){
                            ex.printStackTrace();
                            RbHelper.pesan(getActivity(),"Error ambil data");
                        }
                    }
                });
            }
        });
    }
}