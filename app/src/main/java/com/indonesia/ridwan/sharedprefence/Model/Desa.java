package com.indonesia.ridwan.sharedprefence.Model;

/**
 * Created by hasanah on 9/27/16.
 */
public class Desa extends Kecamatan {
    public String getIdDesa() {
        return idDesa;
    }

    public void setIdDesa(String idDesa) {
        this.idDesa = idDesa;
    }

    public String getNmDesa() {
        return nmDesa;
    }

    public void setNmDesa(String nmDesa) {
        this.nmDesa = nmDesa;
    }

    private String idDesa,nmDesa;
}
