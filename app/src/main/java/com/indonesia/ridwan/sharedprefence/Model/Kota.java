package com.indonesia.ridwan.sharedprefence.Model;

/**
 * Created by hasanah on 9/27/16.
 */
public class Kota {
    public String getIdKota() {
        return idKota;
    }

    public void setIdKota(String idKota) {
        this.idKota = idKota;
    }

    public String getNmKota() {
        return nmKota;
    }

    public void setNmKota(String nmKota) {
        this.nmKota = nmKota;
    }

    private String idKota,nmKota;
}
