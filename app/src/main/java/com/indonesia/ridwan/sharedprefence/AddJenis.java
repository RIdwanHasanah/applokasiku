package com.indonesia.ridwan.sharedprefence;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.indonesia.ridwan.sharedprefence.Helpers.BaseActivity;
import com.indonesia.ridwan.sharedprefence.Helpers.FileUtility;
import com.indonesia.ridwan.sharedprefence.Helpers.ImageCustomize;
import com.indonesia.ridwan.sharedprefence.Helpers.RbHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AddJenis extends BaseActivity {

    private ImageView ivCamera,ivPreview;
    private EditText txtNama,txtJenis;
    private Button btnSimpan;

    private FileUtility fileUtility;
    private final int TAKE_PHOTO = 234;
    private static final int SELECT_PHOTO = 321 ;

    private boolean isImage = false;
    private  String path = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_jenis);

        setupView();
    }

    private void setupView(){

        ivCamera = (ImageView) findViewById(R.id.imageView3);
        ivPreview = (ImageView) findViewById(R.id.imageView2);
        ivCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //tampilkan pilihan image dari mana
                showDialogPilih();
            }
        });

        txtNama = (EditText) findViewById(R.id.eKategori);
        txtJenis = (EditText) findViewById(R.id.eLokasi);

        btnSimpan = (Button) findViewById(R.id.btnSimpan);
        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.startAnimation(animation);
                simpanAction();
            }
        });

    }

    private void showDialogPilih(){
        String[] items = {"Kamera","Galeri"};

        AlertDialog.Builder builder = new AlertDialog.Builder(c);

        builder.setTitle("Pilih Gambar");

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i){
                    case 0:
                        callKamera();
                        break;
                    case 1:
                        callGaleri();
                        break;
                }
            }
        });

        /**Menampilkan Dialog */
        //builder.show();
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    /**Method callGalery untuk mengambil gambar di galery */
    private void callGaleri(){
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        i.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivityForResult(i,SELECT_PHOTO);
    }

    /**Method callKamera untuk mengambil gambar lewat camera*/
    private void callKamera(){
        fileUtility = new FileUtility(c);
        Uri uriSavedImage = Uri.fromFile(fileUtility.getTempJpgImageFile());

        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        i.putExtra(MediaStore.EXTRA_OUTPUT,uriSavedImage);
        i.setFlags(i.FLAG_ACTIVITY_NO_HISTORY);
        startActivityForResult(i,TAKE_PHOTO);
    }

    //untuk handle gambar yang di pilih
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode){
            case TAKE_PHOTO:
                if(resultCode== Activity.RESULT_OK){
                    File imageFile = fileUtility.getTempJpgImageFile();
                    String path = imageFile.getAbsolutePath();

                    //memanggil lokasi file
                    File file = new File(path);
                    File filetmp = ImageCustomize.resizeImage(file);
                    Bitmap bmp = ImageCustomize.decodeFile(filetmp,300);

                    ivPreview.setImageBitmap(bmp);
                    //path = filetmp.getAbsolutePath().toString();
                    isImage = true;
                }else {
                    isImage = false;
                }
                break;

            case SELECT_PHOTO:
                if (resultCode == Activity.RESULT_OK) {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(selectedImage
                            , filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int index = cursor.getColumnIndex(filePathColumn[0]);
                    String pictPath = cursor.getString(index);
                    cursor.close();

                    File f = new File(pictPath);
                    File file1 = ImageCustomize.resizeImage(f);
                    Bitmap bmp = ImageCustomize.decodeFile(file1, 300);

                    ivPreview.setImageBitmap(bmp);

                    path = file1.getAbsolutePath().toString();
                    isImage = true;
                }else {
                    isImage = false;
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }



    public void simpanAction(){

        txtNama.setError(null);
        txtJenis.setError(null);

        //lakukan proses pengecekan semua field sudah disi dengan benar
        boolean cancel = false;
        View focusView= null;

        if (RbHelper.isEmpty(txtNama)){

            txtNama.setError("Kategori harus disisi");
            focusView  = txtNama;
            cancel = true;
        }else if(RbHelper.isEmpty(txtJenis)){
            txtJenis.setError("Lokai harus diisi");
            focusView = txtJenis;
            cancel = true;
        }


        //cek apakah ada yang belum diisi
        if (cancel){
            focusView.requestFocus();
        }else {

            String url = RbHelper.BASE_URL + "jenis/tambah";

            //siapkan parameter yang akan di kirim
            RequestBody formbody = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("f_nama",txtNama.getText().toString())
                    .addFormDataPart("f_idUser",sesi.getIdUser())
                    .addFormDataPart("f_kategori",txtJenis.getText().toString())
                    .addFormDataPart("userfile","kategori_foto.png",
                            RequestBody.create(MediaType.parse("image/png"),new File(path)))
                    .build();

            //buat request yang akan di kirim keserver
            final Request request = new Request.Builder()
                    .url(url)
                    .post(formbody)
                    .build();


            //debuging data yang dikirimkan

            RbHelper.pre("url : "+url+",parameter"+RbHelper.bodyToString(formbody));

            //tampilkan loadingnya
            showLoading();

            //kirimkan ke server
            okhttp.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, final IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                hideLoading();
                                RbHelper.pesan(c,"Error get data" + e.getMessage());
                            }catch (Exception ex){


                            }
                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                hideLoading();
                            }catch (Exception e){

                            }
                        }
                    });

                    if (!response.isSuccessful()){
                        throw new IOException("Unexpected code"+response);
                    }

                    //baca response dari server
                    final String reponData = response.body().string();
                    RbHelper.pre("respon "+reponData);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                JSONObject json = new JSONObject(reponData);

                                //cek resultnya
                                boolean hasil = json.getBoolean("result");
                                String msg = json.getString("msg");

                                RbHelper.pesan(c,msg);

                                if (hasil){

                                    //create sesion loginya


                                    Intent i = new Intent(c,MainActivity.class);
                                    startActivity(i);
                                    finish();
                                }
                            }catch (JSONException e){
                                e.printStackTrace();
                                RbHelper.pesan(c,"Error pasrsing Json" + e.getMessage());
                            }catch (Exception e){
                                e.printStackTrace();
                                RbHelper.pesan(c,"Error get data" + e.getMessage());
                            }
                        }
                    });

                }
            });

        }
    }
}
