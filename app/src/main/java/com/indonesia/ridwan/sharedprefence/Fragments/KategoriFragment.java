package com.indonesia.ridwan.sharedprefence.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indonesia.ridwan.sharedprefence.Adapter.KategoriAdapter;
import com.indonesia.ridwan.sharedprefence.Helpers.RbHelper;
import com.indonesia.ridwan.sharedprefence.Model.Kategori;
import com.indonesia.ridwan.sharedprefence.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class KategoriFragment extends Fragment {
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView rvData;
    private ArrayList<Kategori> data;
    private KategoriAdapter adapter;


    public KategoriFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_kategori, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setUpView(view);
        getData();
    }

    private void setUpView(View v){
        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipeRefresh);
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        swipeRefreshLayout.setRefreshing(false);
                        getData();
                    }
                });
            }
        });
        rvData = (RecyclerView) v.findViewById(R.id.rvData);
        rvData.setLayoutManager(new LinearLayoutManager(getActivity()));

        data = new ArrayList<>();
    }

    private void getData(){
        data.clear();

        String url = RbHelper.BASE_URL+"kategori";

        //Create request okHtttp3
        Request request = new Request.Builder()
                .url(url)
                .build();

        RbHelper.pre("url : "+url);

        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
            }
        });

        OkHttpClient okHttpClient = new OkHttpClient();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
                RbHelper.pesan(getActivity(),"Error Connection"+e.getMessage());
            }

            @Override
            public void onResponse(final Call call, Response response) throws IOException {
                swipeRefreshLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });

                //chek hasil response
                if(!response.isSuccessful()){
                    throw new IOException("Unexpected code"+response);
                }
                //read data
                final String resposeData = response.body().string();
                RbHelper.pre("respone: "+resposeData );

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject json = new JSONObject(resposeData);
                            //chek result
                            boolean hasil = json.getBoolean("result");
                            String msg = json.getString("msg");

                            if (hasil){
                                JSONArray jsonArray = json.getJSONArray("data");
                                for (int i=0;i<jsonArray.length();i++){
                                    JSONObject jObj = jsonArray.getJSONObject(i);

                                    Kategori x = new Kategori();
                                    x.setIdkategori(jObj.getString("id_kategori"));
                                    x.setKategoriNama(jObj.getString("kategori_nama"));
                                    x.setKategoriImage(jObj.getString("kategori_image"));

                                    data.add(x);
                                }
                            }

                            adapter = new KategoriAdapter(getActivity(),data);
                            rvData.setAdapter(adapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            RbHelper.pesan(getActivity(),"Error Parsing Json");
                        }catch (Exception e){
                            e.printStackTrace();
                            RbHelper.pesan(getActivity(),"Error ambil data");
                        }
                    }
                });
            }
        });
    }
}
