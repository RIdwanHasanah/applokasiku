package com.indonesia.ridwan.sharedprefence.Model;

/**
 * Created by hasanah on 9/27/16.
 */
public class Jenis extends Kategori {
    private  String idJenis;

    public String getJenisNama() {
        return jenisNama;
    }

    public void setJenisNama(String jenisNama) {
        this.jenisNama = jenisNama;
    }

    public String getIdJenis() {
        return idJenis;
    }

    public void setIdJenis(String idJenis) {
        this.idJenis = idJenis;
    }

    public String getJenisImage() {
        return jenisImage;
    }

    public void setJenisImage(String jenisImage) {
        this.jenisImage = jenisImage;
    }

    private String jenisNama;
    private String jenisImage;
}
