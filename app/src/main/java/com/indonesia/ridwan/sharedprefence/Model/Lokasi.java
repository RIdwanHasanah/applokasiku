package com.indonesia.ridwan.sharedprefence.Model;

/**
 * Created by hasanah on 9/22/16.
 */
public class Lokasi {

    String idLokasi;
    String lokasiNama;
    String lokasiImg;
    String lokasiDesa;
    String lokasiSeen;
    String nmWisata;
    String lalitude;
    String longlitude;
    String marker;

    public String getIdJenis() {
        return idJenis;
    }

    public void setIdJenis(String idJenis) {
        this.idJenis = idJenis;
    }

    String idJenis;

    public String getMarker() {
        return marker;
    }

    public void setMarker(String marker) {
        this.marker = marker;
    }

    public String getLalitude(){
        return lalitude;
    }
    public void setLalitude(String lalitude){
        this.lalitude = lalitude;
    }

    public String getLonglitude(){
        return longlitude;
    }

    public void setLonglitude(String longlitude){
        this.longlitude = longlitude;
    }

    public String getLokasiImg() {
        return lokasiImg;
    }

    public void setLokasiImg(String lokasiImg) {
        this.lokasiImg = lokasiImg;
    }

    public String getIdLokasi() {
        return idLokasi;
    }

    public void setIdLokasi(String idLokasi) {
        this.idLokasi = idLokasi;
    }

    public String getLokasiNama() {
        return lokasiNama;
    }

    public void setLokasiNama(String lokasiNama) {
        this.lokasiNama = lokasiNama;
    }

    public String getLokasiDesa() {
        return lokasiDesa;
    }

    public void setLokasiDesa(String lokasiDesa) {
        this.lokasiDesa = lokasiDesa;
    }

    public String getLokasiSeen() {
        return lokasiSeen;
    }

    public void setLokasiSeen(String lokasiSeen) {
        this.lokasiSeen = lokasiSeen;
    }

    public String getNmWisata() {
        return nmWisata;
    }

    public void setNmWisata(String nmWisata) {
        this.nmWisata = nmWisata;
    }


}
