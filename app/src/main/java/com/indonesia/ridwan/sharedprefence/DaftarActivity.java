package com.indonesia.ridwan.sharedprefence;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.indonesia.ridwan.sharedprefence.Helpers.BaseActivity;
import com.indonesia.ridwan.sharedprefence.Helpers.RbHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class DaftarActivity extends BaseActivity {
    private EditText txtNama,txtEmail,txtPassword;
    private Button btnBatal,btnDaftar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar);

        setupView();
    }


    private void setupView(){
        //Memnghubungkan id XMl dengan Id java
        txtNama = (EditText) findViewById(R.id.txtnama);
        txtEmail = (EditText) findViewById(R.id.txtemail);
        txtPassword = (EditText) findViewById(R.id.txtpassword);

        btnBatal = (Button) findViewById(R.id.btnbatal);
        btnDaftar = (Button) findViewById(R.id.btndaftar);
        btnBatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                daftatarAction();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(getBaseContext(),LoginActivity.class);
        startActivity(i);
        finish();
    }

    private void daftatarAction(){
        txtNama.setError(null);
        txtEmail.setError(null);
        txtPassword.setError(null);

        //lakukan proses pengecekan semua field sudah disi dengan benar
        //membuat variable agar bisa di panggil
        boolean cancel = false;
        View focusView = null;

        //jika txtNama kosong maka setError dan menampilkan text
        /*Memakai method isEmpty yg berada di class RbHelper tanpa membuat objek kelas karna
         method isEmpty adalah static lalu mengisi parameter dengan isEmpty dengan editText sesuai dengan
         parameter method isEmpty*/
        if (RbHelper.isEmpty(txtNama)){
            //memberikan pesan jika edittext error
            txtNama.setError("Nama Harus diisi");
            /*focusView adalah method View lihat Variable di atas berisi null lalu di
            * sini di isi oleh edittext maka focusview akan pada focus pada edittext yang kosong walaupun
            * mengisi edittext yang lain */
            focusView = txtNama;
            /**cancel adalah boolean yang berisi nilai false maka jika cancel adalah true maka cancel=false
            * jadi jika edit text kosong maka akan memuculkan text*/
            cancel = true;
        }
        //jika txtEmail kosong maka setError dan menampilkan text
        else if (RbHelper.isEmpty(txtEmail)){

            txtEmail.setError("Email Harus diisi");
            focusView = txtEmail;
            cancel = true;
        }
        //jika txtPassword kosong maka setError dan menampilkan text
        else if (RbHelper.isEmpty(txtPassword)){

            txtPassword.setError("Pasword Harus diisi");
            focusView = txtPassword;
            cancel = true;
        }

        //cek apakah ada yg belom disisi

        //jika false maka akan focus pada error di edittext
        if (cancel){
            focusView.requestFocus();
        }
        //jika tidak error maka parsing ke server
        else{
            //RbHelper.pesan(getBaseContext(),"Berhasil Daftar");
            //membuat variable String url yang berisi link url yang ada di class Rbhelper
            //String url = link yang ada di RbHelper di tambah "user/daftar"
            String url = RbHelper.BASE_URL + "user/daftar";
            //siapkan parameter yang akan di kirim

            //RequestBody adalah kelas abstaract yang berisi method dari gradle Okhttp yang sudah di syncron
            /*FromBody adalah kelas public final dan telah extends class RequestBody yang berisi method dari
             gradle Okhttp yang sudah di syncron*/
            RequestBody formBody = new FormBody.Builder()
                    //menambahkan ke server dgn mengambilnya dari edittext dan di ubah ke dalam bentuk String
                    .add("f_nama",txtNama.getText().toString())
                    .add("f_email",txtEmail.getText().toString())
                    .add("f_password",txtPassword.getText().toString())
                    .build();

            //buat request yang akan di kirim keserver
            /*Request adalah Class dan Builder adalah method dari class Request*/
            final Request request = new Request.Builder() //membuat objek Class Request
                    //karna ingin mengirim data dari android ke server maka menggunakan post
                    .url(url)
                    .post(formBody)
                    .build();

            //debuging data yang dikirimkan
            //berfungsi menampilkan log error yang di dapat dari server
            RbHelper.pre("url : "+url+", parameter : "+RbHelper.bodyToString(formBody));

            //tampilkan loadingnya
            showLoading();

            //krimkan keserver
            okhttp.newCall(request).enqueue(new Callback() {

                //untuk melihat error jaringan
                @Override
                public void onFailure(Call call, final IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                hideLoading();
                                RbHelper.pesan(c,"error get data " + e.getMessage());
                            }catch (Exception ex){

                            }
                        }
                    });

                }

                //untuk menerima respon dari server
                @Override
                public void onResponse(final Call call, Response response) throws IOException {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                hideLoading();
                            }catch (Exception ex){

                            }
                        }
                    });

                    if (!response.isSuccessful()){
                        throw new IOException("Unexpected code " + response);
                    }

                    //baca response dari server
                    final String reseponData = response.body().string();
                    RbHelper.pre("response : " + reseponData);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject json  = new JSONObject(reseponData);

                                //check resultnya
                                boolean hasil = json.getBoolean("result");
                                String msg = json.getString("msg");

                                RbHelper.pesan(c,msg); //Toast buatan mas Riyadi yang ada di Class RbHelper

                                if (hasil){
                                    Intent intent = new Intent(c,LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }catch (JSONException e){
                                e.printStackTrace();
                                RbHelper.pesan(c,"Error parsing Json" + e.getMessage());
                            }catch (Exception e){
                                e.printStackTrace();
                                RbHelper.pesan(c,"Error get data" + e.getMessage());
                            }
                        }
                    });

                }
            });
        }

    }
}
