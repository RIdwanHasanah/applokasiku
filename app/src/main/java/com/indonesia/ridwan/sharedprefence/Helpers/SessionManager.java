package com.indonesia.ridwan.sharedprefence.Helpers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by hasanah on 9/20/16.
 */
public class SessionManager {
    private static final String ID_USER = "id_user";
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context c;

    private static  final String PREF_NAME = "AppLokasiku",
    IS_LOGIN = "isLogin", NAMA = "nama", EMAIL = "email",SESSION = "session";

    //buat sebuah contstruktor
    public  SessionManager (Context c){
        this.c=c;
        pref = c.getSharedPreferences(PREF_NAME,0);
        editor = pref.edit();
    }

    public void createSession(String v){
        editor.putString(SESSION,v);
        editor.putBoolean(IS_LOGIN,true);
        editor.commit();
    }

    //buat getter setter untuk nama dan Email
    public void setNama (String v){
        editor.putString(NAMA,v);
        editor.commit();
    }

    public String getNama(){
        return pref.getString(NAMA,"");
    }

    public void setEmail(String v){
        editor.putString(EMAIL,v);
        editor.commit();
    }

    public String getEmail(){
        return  pref.getString(EMAIL,"");
    }


    //check status login

    public Boolean islogin(){
        return pref.getBoolean(IS_LOGIN,false);
    }

    public String getSesi (){
        return pref.getString(SESSION,"");
    }

    //funtion logout
    public void logout(){
        editor.clear();
        editor.commit();
    }

    public void setIdUser(String v) {
        editor.putString(ID_USER,v);
        editor.commit();
    }
    public String getIdUser(){
        return pref.getString(ID_USER,"");
    }
}
