package com.indonesia.ridwan.sharedprefence.Model;

/**
 * Created by hasanah on 9/23/16.
 */
public class Peta {
    String lalitude;
    String longlitude;

    public String getLalitude(){
        return lalitude;
    }
    public void setLalitude(String lalitude){
        this.lalitude = lalitude;
    }

    public String getLonglitude(){
        return longlitude;
    }

    public void setLonglitude(String longlitude){
        this.longlitude = longlitude;
    }
}
