package com.indonesia.ridwan.sharedprefence.Helpers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.AlphaAnimation;

import com.indonesia.ridwan.sharedprefence.AddJenis;
import com.indonesia.ridwan.sharedprefence.AddKategoriActivity;
import com.indonesia.ridwan.sharedprefence.AddLokasi;
import com.indonesia.ridwan.sharedprefence.LoginActivity;
import com.indonesia.ridwan.sharedprefence.MainActivity;
import com.indonesia.ridwan.sharedprefence.R;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

/**
 * Created by hasanah on 9/20/16.
 */
public class BaseActivity extends AppCompatActivity {

    protected OkHttpClient okhttp;
    protected Context c;
    protected AlphaAnimation animation = new AlphaAnimation(1F, 0.6F);
    protected ProgressDialog dialog;
    protected SessionManager sesi;

    //unutk menu drawer
    private DrawerBuilder drawerBuilder = null;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        c= this;
        okhttp = new OkHttpClient.Builder()
                .connectTimeout(300, TimeUnit.SECONDS)
                .writeTimeout(300,TimeUnit.SECONDS)
                .readTimeout(300,TimeUnit.SECONDS)
                .build();

        dialog = new ProgressDialog(c);

        sesi = new SessionManager(c);
    }

    protected void showLoading (){
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setInverseBackgroundForced(false);
        dialog.setMessage("Loading...");

        dialog.show();
    }

    protected void hideLoading(){
        if (dialog!=null)
            dialog.dismiss();
    }

    //method unutk menu sliding/drawer
    protected void menuDrawer(int posisi){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //buat item unutk menu drawer
        SecondaryDrawerItem home = new SecondaryDrawerItem().withName("Home").withTag("home").withIcon(R.drawable.home);
        SecondaryDrawerItem kategori = new SecondaryDrawerItem().withName("Kategori").withTag("kategori").withIcon(R.drawable.category);
        SecondaryDrawerItem lokasi = new SecondaryDrawerItem().withName("Lokasi").withTag("lokasi").withIcon(R.drawable.ic_lokasi);
        SecondaryDrawerItem logout = new SecondaryDrawerItem().withName("Logout").withTag("logout").withIcon(R.drawable.exit);
        SecondaryDrawerItem jenis = new SecondaryDrawerItem().withName("Jenis").withTag("jenis").withIcon(R.drawable.riparianhabitat);

        //unutk header slide
        AccountHeader header = new AccountHeaderBuilder()
                .withActivity(this).withHeaderBackground(R.drawable.natural).withTextColor(getResources().getColor(R.color.material_drawer_primary_text))
                .addProfiles(
                        new ProfileDrawerItem().withName(sesi.getNama()).withEmail(sesi.getEmail())
                                .withIcon(getResources().getDrawable(R.drawable.profilelll))
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                        return false;
                    }
                })
                .build();



        drawerBuilder = new DrawerBuilder();
        drawerBuilder.withAccountHeader(header)
                .withActivity(this)
                .withToolbar(toolbar);
        //tambahkan item ke drawer
        drawerBuilder.addDrawerItems(home,kategori,lokasi,jenis,logout)
                .withSelectedItemByPosition(posisi).build();
        //tambahkan action ke drawer
        drawerBuilder.withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
            @Override
            public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                Intent i =null;
                String tag = (String) drawerItem.getTag();

                switch (tag){
                    case "home":
                        i = new Intent(c, MainActivity.class);
                        startActivity(i);
                        finish();
                        break;
                    case "kategori":
                        i = new Intent(c, AddKategoriActivity.class);
                        startActivity(i);
                        finish();
                        break;
                    case "lokasi":
                        i = new Intent(c, AddLokasi.class);
                        startActivity(i);
                        finish();
                        break;
                    case "jenis":
                        i = new Intent(c, AddJenis.class);
                        startActivity(i);
                        finish();
                        break;
                    case "logout":
                        sesi.logout();
                        i = new Intent(c, LoginActivity.class);
                        startActivity(i);
                        finish();
                        break;


                }
                return true;
            }
        });





    }
}
