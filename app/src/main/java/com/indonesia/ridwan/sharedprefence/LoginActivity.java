package com.indonesia.ridwan.sharedprefence;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.indonesia.ridwan.sharedprefence.Helpers.BaseActivity;
import com.indonesia.ridwan.sharedprefence.Helpers.RbHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LoginActivity extends BaseActivity {
    private EditText txtUsername,txtPassword;
    private TextView tvDaftar;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setupView();
    }

    private void setupView(){
        //koneksikan Variable dgn id xml
        txtUsername = (EditText) findViewById(R.id.txtUSername);
        txtPassword = (EditText) findViewById(R.id.txtPass);
        btnLogin = (Button) findViewById(R.id.btnlogin);
        tvDaftar = (TextView) findViewById(R.id.tvDaftar);
        //action saat text fi klik
        tvDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //untuk efek spt klik
                AlphaAnimation animation  = new AlphaAnimation(1F,0.7F);
                view.startAnimation(animation);

                //pindahkan ke Halaman Daftar
                Intent i = new Intent(getBaseContext(),DaftarActivity.class);
                startActivity(i);
                finish();
            }
        });

        clickBtnLogin();
    }

    public void clickBtnLogin(){
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ActionLogin();

            }
        });
    }

    public void ActionLogin(){

        txtPassword.setError(null);
        txtUsername.setError(null);

        //lakukan proses pengecekan semua field sudah disi dengan benar
        boolean cancel = false;
        View focusView= null;

        if (RbHelper.isEmpty(txtUsername)){

            txtUsername.setError("Email harus disisi");
            focusView  = txtUsername;
            cancel = true;
        }else if (RbHelper.isEmpty(txtPassword)){

        txtPassword.setError("Passwor Harus Diisi");
        focusView = txtPassword;
        cancel= true;
        }


        //cek apakah ada yang belum diisi
        if (cancel){
            focusView.requestFocus();
        }else {

            String url = RbHelper.BASE_URL + "user/login";

            //siapkan parameter yang akan di kirim
            RequestBody formbody = new FormBody.Builder()
                    .add("f_email",txtUsername.getText().toString())
                    .add("f_password",txtPassword.getText().toString())
                    .build();

            //buat request ayang akan di kirim keserver
            final Request request = new Request.Builder()
                    .url(url)
                    .post(formbody)
                    .build();


            //debuging data yang dikirimkan

            RbHelper.pre("url : "+url+",parameter"+RbHelper.bodyToString(formbody));

            //tampilkan loadingnya
            showLoading();

            //kirimkan ke server
            okhttp.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, final IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                hideLoading();
                                RbHelper.pesan(c,"Error get data" + e.getMessage());
                            }catch (Exception ex){


                            }
                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                hideLoading();
                            }catch (Exception e){

                            }
                        }
                    });

                    if (!response.isSuccessful()){
                        throw new IOException("Unexpected code"+response);
                    }

                    //baca response dari server
                    final String reponData = response.body().string();
                    RbHelper.pre("respon "+reponData);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                JSONObject json = new JSONObject(reponData);

                                //cek resultnya
                                boolean hasil = json.getBoolean("result");
                                String msg = json.getString("msg");

                                RbHelper.pesan(c,msg);

                                if (hasil){

                                    //create sesion loginya
                                    String token = json.getString("token");
                                    JSONObject jsonObject = json.getJSONObject("data");

                                    sesi.createSession(token);
                                    sesi.setNama(jsonObject.getString("user_nama"));
                                    sesi.setEmail(jsonObject.getString("user_email"));
                                    sesi.setIdUser(jsonObject.getString("id_user"));

                                    Intent i = new Intent(c,MainActivity.class);
                                    startActivity(i);
                                    finish();
                                }
                            }catch (JSONException e){
                                e.printStackTrace();
                                RbHelper.pesan(c,"Error pasrsing Json" + e.getMessage());
                            }catch (Exception e){
                                e.printStackTrace();
                                RbHelper.pesan(c,"Error get data" + e.getMessage());
                            }
                        }
                    });

                }
            });

        }
    }
}
