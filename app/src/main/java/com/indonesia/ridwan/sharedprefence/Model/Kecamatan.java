package com.indonesia.ridwan.sharedprefence.Model;

/**
 * Created by hasanah on 9/27/16.
 */
public class Kecamatan extends Kota {

    private String idKecamatan,namaKecamatan;

    public String getIdKecamatan() {
        return idKecamatan;
    }

    public void setIdKecamatan(String idKecamatan) {
        this.idKecamatan = idKecamatan;
    }

    public String getNamaKecamatan() {
        return namaKecamatan;
    }

    public void setNamaKecamatan(String namaKecamatan) {
        this.namaKecamatan = namaKecamatan;
    }
}
