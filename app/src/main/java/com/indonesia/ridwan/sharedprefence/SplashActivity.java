package com.indonesia.ridwan.sharedprefence;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.indonesia.ridwan.sharedprefence.Helpers.SessionManager;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                //setelah di tahan 2 detik ke halamn login

                SessionManager sesi = new SessionManager(getApplicationContext());

                if (sesi.islogin()){
                    Intent i = new Intent(getBaseContext(),MainActivity.class);
                    startActivity(i);
                    finish();
                }else {

                Intent i  = new Intent(getBaseContext(),LoginActivity.class);
                startActivity(i);
                finish();
                }
            }
        },2000L); //waktu hold
    }
}
